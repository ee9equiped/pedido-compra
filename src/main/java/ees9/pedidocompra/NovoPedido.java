package ees9.pedidocompra;

import entities.Repository;

public class NovoPedido implements IStatus {

	@Override
	public void aceitar(Pedido pedido) {
		
		if(!(pedido.getNumeroDeItens() >=1)) {
			throw new IllegalStateException("O pedido deve conter pelo menos 1 item.");
		}
		if (!pedido.isQuantidadeOk()) {
			throw new IllegalStateException("Quantia total maior do que o limite permitido.");
		}

		if (!pedido.isLimiteDeCreditoOk()) {
			throw new IllegalStateException("Limite de crédito excedido.");
		}
		pedido.setStatus(Status.Aceito);
		Repository.getInstance().add(pedido);
	}

	@Override
	public void pagar(Pedido pedido) {
		throw new IllegalStateException("Este pedido ainda não foi aceito.");
	}

	@Override
	public void rejeitar(Pedido pedido) {
		pedido.setStatus(Status.Cancelado);
	}

}
