package ees9.pedidocompra;

public interface IStatus {

	public void aceitar(Pedido pedido);

	public void pagar(Pedido pedido);

	public void rejeitar(Pedido pedido);

}
