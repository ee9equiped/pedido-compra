package ees9.pedidocompra;

import entities.annotations.ActionDescriptor;
import entities.annotations.PropertyDescriptor;
import entities.annotations.View;
import entities.annotations.Views;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
@EqualsAndHashCode(of = "id")
@Views(
	{
		@View(
			title = "Lista de Pedidos",
			name = "ListaDePedidos",
			filters = "cliente",
			members = "id,data,numeroDeItens,quantiaTotal,status,teste()",
			namedQuery = "From ees9.pedidocompra.Pedido order by id",
			rows = 10,
			template = "@FILTER+@CRUD+@TABLE+@PAGER"
		),
		@View(title = "Novo Pedido",
			name = "NovoPedido",
			members = "[Header[#id,#data;#cliente:2]; "
			+ " Itens[adicionarItem(); "
			+ " itens<#produto:3;#quantidade,#valor;remove()>;"
			+ " *numeroDeItens]; "
			+ " aceitar()] ",
			namedQuery = "Select new ees9.pedidocompra.Pedido()",
			template = "@TABLE+@PAGER"
		),
		@View(title = "Visualizar Pedido",
		name = "VisualizarPedido",
		members = "[Header[#id,#data;#cliente:2;status]; "
		+ " Itens[adicionarItem(); "
		+ " itens<#produto:3;#quantidade,#valor;remove()>;"
		+ " *numeroDeItens]; "
		+ " [pagar(), rejeitar()]] ",
		namedQuery = "From ees9.pedidocompra.Pedido order by id",
		template = "@TABLE+@PAGER",
		rows = 1
	)
	}
)
public class Pedido implements Serializable {

	private static final long serialVersionUID = 9193302837267958997L;
	@Version
	private Timestamp version;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@PropertyDescriptor(readOnly=true, secret = true)
	private Long id;
	@NotNull
	private Date data;
	@ManyToOne(optional = false)
	@NotNull(message = "Informe o cliente do pedido.")
	private Cliente cliente;
	@OneToMany(mappedBy = "pedido", cascade = CascadeType.ALL, orphanRemoval = true)
	@Valid
	private List<Item> itens;
	@Column(precision = 4)
	private Integer numeroDeItens;
	@Column(precision = 8, scale = 2)
	private Double quantiaTotal;
	@Enumerated
	private Status status;
	
	public Pedido() {
		data = new Date();
		itens = new ArrayList<Item>();
		numeroDeItens = 0;
		quantiaTotal = 0.0;
		status = Status.NovoPedido;
	}
	
	@ActionDescriptor(immediate = false, refreshView = false)
	public void adicionarItem() {
		Item item = new Item();
		item.setPedido(this);
		itens.add(item);
		numeroDeItens++;
		atualizarQuantiaTotal();
	}
	
	public String teste() {
		return "go:ees9.pedidocompra.Pedido@NovoPedido";
	}

	protected void atualizarQuantiaTotal() {
		Double quantiaTotalDosItens = 0.0;
		for (Item item : itens) {
			quantiaTotalDosItens += item.getQuantiaTotal();
		}
		quantiaTotal = quantiaTotalDosItens;
	}

	public String aceitar() {
		status.aceitar(this);
		Repositorio.save(this);
		Repositorio.getInstance().persistAll();
		return "Pedido aceito.";
	}

	public String pagar() {
		status.pagar(this);
		Repositorio.save(this);
		return "Pedido pago.";
	}

	public String rejeitar() {
		status.rejeitar(this);
		Repositorio.save(this);
		return "Pedido cancelado.";
	}

	protected boolean isQuantidadeOk() {
		// TODO Configurar o valor '1000' globalmente
		for (Item item : itens) {
			quantiaTotal += item.getQuantiaTotal();
		}
		return (quantiaTotal < 1000);
	}

	protected boolean isLimiteDeCreditoOk() {
		return quantiaTotal < cliente.getLimiteDeCredito();
	}

}
