package ees9.pedidocompra;

import entities.annotations.View;
import entities.annotations.Views;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.br.CNPJ;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Data
@EqualsAndHashCode(of = "id")
@Views({ @View(name = "Cliente", title = "Cliente", members = "[nome;cnpj;cidade;limiteDeCredito]", template = "@CRUD_PAGE") })
public class Cliente implements Serializable {

	private static final long serialVersionUID = -3358399645412650465L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotNull
	private String nome;
	@NotNull
	@CNPJ
	private String cnpj;
	private String cidade;
	@NotNull
	private Double limiteDeCredito;
	
	@Override
	public String toString() {
		return nome;
	}

}
