package ees9.pedidocompra;

import entities.annotations.View;
import entities.annotations.Views;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@EqualsAndHashCode(of = "id")
@Data
@Views({ @View(name = "Produto", title = "Produto", members = "[descricao]", template = "@TABLE+@CRUD+@PAGER") })
public class Produto implements Serializable {

	private static final long serialVersionUID = 6558242749178152270L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotNull
	private String descricao;
	
	@Override
	public String toString() {
		return descricao;
	}
}
