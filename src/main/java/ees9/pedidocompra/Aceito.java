package ees9.pedidocompra;

public class Aceito implements IStatus {

	@Override
	public void aceitar(Pedido pedido) {
		throw new IllegalStateException("Este pedido já foi aceito.");
	}

	@Override
	public void pagar(Pedido pedido) {
		pedido.setStatus(Status.Pago);
	}

	@Override
	public void rejeitar(Pedido pedido) {
		pedido.setStatus(Status.Cancelado);
	}

}
