package ees9.pedidocompra;

public class Pago implements IStatus {

	@Override
	public void aceitar(Pedido pedido) {
		throw new IllegalStateException("Este pedido está pago.");	
	}

	@Override
	public void pagar(Pedido pedido) {
		throw new IllegalStateException("Este pedido está pago.");	
	}

	@Override
	public void rejeitar(Pedido pedido) {
		pedido.setStatus(Status.Cancelado);
	}

}