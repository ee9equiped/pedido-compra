package ees9.pedidocompra;

import entities.annotations.ActionDescriptor;
import entities.annotations.EntityDescriptor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Data
@EqualsAndHashCode(of = {"id"})
@EntityDescriptor(hidden = true, pluralDisplayName = "Itens")
public class Item implements Serializable {

	private static final long serialVersionUID = 7267623179993017507L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@ManyToOne(optional = false)
	private Pedido pedido;
	@ManyToOne(optional = false)
	@NotNull(message = "Informe o produto do pedido.")
	private Produto produto;
	@NotNull
	@Min(value = 0, message = "O preço precisa ser maior ou igual a zero.")
	private Double valor;
	@NotNull
	@Min(value = 1, message = "A quantidade de produtos precisa ser pelo menos um.")
	private Integer quantidade;

	public Item() {
		valor = 0.0;
		quantidade = 0;
	}

	public double getQuantiaTotal() {
		return valor * quantidade;
	}
	
	@ActionDescriptor(displayName = "Remover", value = "Remover")
    public void remove() {
    	pedido.getItens().remove(this);
    	pedido.setNumeroDeItens(pedido.getNumeroDeItens()-1);
    	pedido.atualizarQuantiaTotal();
    }
}