package ees9.pedidocompra;

public enum Status implements IStatus {

	NovoPedido(new NovoPedido()), Aceito(new Aceito()), Pago(new Pago()), Cancelado(new Cancelado());

	private IStatus status;

	Status(IStatus status) {
		this.status = status;
	}

	@Override
	public void aceitar(Pedido pedido) {
		status.aceitar(pedido);
	}

	@Override
	public void pagar(Pedido pedido) {
		status.pagar(pedido);
	}

	@Override
	public void rejeitar(Pedido pedido) {
		status.rejeitar(pedido);
	}
}