package ees9.pedidocompra;

public class Cancelado implements IStatus {

	@Override
	public void aceitar(Pedido pedido) {
		throw new IllegalStateException("Este pedido está cancelado.");	
	}

	@Override
	public void pagar(Pedido pedido) {
		throw new IllegalStateException("Este pedido está cancelado.");	
	}

	@Override
	public void rejeitar(Pedido pedido) {
		throw new IllegalStateException("Este pedido está cancelado.");	
	}

}
